function T=total(C,a,b,c,d,e,f)
T=(a*C(1))+(b*C(2))+(c*C(3))+(d*C(4))+(e*C(5))+(f*C(6));
%function gives total amount of coins in cents 
