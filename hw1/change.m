clear
C=[100 50 25 10 5 1];
%C is a vector containing the different values of the coins in descending
%order
a=0;
b=0;
c=0;
d=0;
e=0;
f=0;
% variables a-f represent the respective number of each coin used to make
% the total change
D=200;
%D is the total amount of change you are trying to comprise
amax=floor(D/C(1));
%this is the maximum number of coins with the highest value that can be used
%floor guarantees that this is the highest integer possible such that
%amax*C(1)<D
i=0;
%counter for the number of possible ways to make change
for a=amax:-1:0
    %Starts the count for C(1) in this case the dollar coin at the maximum
    %number of coins that will make two dollars
    if total(C,a,0,0,0,0,0)==D;
        i=i+1;
        %the sum using only dollar coins is verified and counted as an
        %option
        continue 
        %the count for the dollar coins is then lowered by one and the
        %maximum number of the next coin is found and so on and so on. 
    else bmax=floor((D-total(C,a,0,0,0,0,0))/C(2));
        %the maximum for b is calculated by subtracting the value of the
        %current number of dollar coins subtracted from D=200
        for b=bmax:-1:0
            if total(C,a,b,0,0,0,0)==D;
                i=i+1;
                continue
            else cmax=floor((D-total(C,a,b,0,0,0,0))/C(3));
                for c=cmax:-1:0
            if total(C,a,b,c,0,0,0)==D;
                i=i+1;
                continue
            else dmax=floor((D-total(C,a,b,c,0,0,0))/C(4));
                for d=dmax:-1:0
            if total(C,a,b,c,d,0,0)==D;
                i=i+1;
                continue
             else emax=floor((D-total(C,a,b,c,d,0,0))/C(5));
                for e=emax:-1:0
            if total(C,a,b,c,d,e,0)==D;
                i=i+1;
                continue
            else f=(D-total(C,a,b,c,d,e,0))/C(6);
                if total(C,a,b,c,d,e,f)==D
                    i=i+1;
                end
            end
                end
            end
                end
            end
                end
            end
        end
    end
end
i
%i is equal to the total number of ways change can be made        
